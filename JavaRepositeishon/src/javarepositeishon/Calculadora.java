/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javarepositeishon;

/**
 *
 * @author Ezequiel
 */
public class Calculadora {

    /**
     * @param valor1
     * @param valor2
     * @param resultado
     * @return 
     */
    public static int Operacion (int valor1,int valor2,int resultado){
       int x = 0;
       if (valor1 + valor2 == resultado){
           x=1;
       }
       
      else if (valor1 - valor2 == resultado){
           x=2;
       }
      else if (valor1 * valor2 == resultado){
           x=3;
       }
      else if (valor1 / valor2 == resultado){
           x=4;
       }
        return x;
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(PruebaC.suite());
       
    }
    
}
