/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javarepositeishon;

import junit.framework.JUnit4TestAdapter;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Ezequiel
 */
public class PruebaC {
    Calculadora calcu = new Calculadora();
    @Test 
    public void Probando(){   
//1=suma. 2=resta. 3=multiplicación. 0=división, 0=error
                  
        assertEquals(Calculadora.Operacion(10, 2, 12), 1);
        assertEquals(Calculadora.Operacion(10, 2, 8), 2); 
        assertEquals(Calculadora.Operacion(10, 2, 20), 3); 
        assertEquals(Calculadora.Operacion(10, 2, 5), 4); 
    
}
    public static junit.framework.Test suite(){
        return new JUnit4TestAdapter(PruebaC.class);
    }
}
