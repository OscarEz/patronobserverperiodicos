/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Observer;

/**
 *
 * @author Ezequiel
 */
public class Main {
    
     
    public static void main (String args[]){
     Periodico pd = new Periodico();
     Subscriptor sa= new Subscriptor();
     Subscriptor sb= new Subscriptor();
     Subscriptor sc= new Subscriptor();
     Subscriptor sd= new Subscriptor();
     Subscriptor sf= new Subscriptor();
     
     pd.addObserver(sa);
     pd.addObserver(sb);
     pd.addObserver(sc);
     pd.addObserver(sd);
     pd.addObserver(sf);
     
     
     pd.entregar();
     System.out.println(" ");
     pd.removeObserver(sb);
     pd.entregar();
        
    }
}
